INTRODUCTION
------------

The Commerce Product Bundle License is a bridge module to achieve the 
Commerce License feature for the Commerce Bundle Products. This module 
has a dependency between these two modules.

The License configured under each product variations. The Product 
and respective Variation type will be configured for the Bundle Product. 

REQUIREMENTS
------------

This module requires the following modules:

 * Commerce License (https://drupal.org/project/commerce_license)
 * Commerce Product Bundle (https://drupal.org/project/commerce_product_bundle)

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules for
further information.

CONFIGURATION
------------

No specific seprate configuration for the Module.

MAINTAINERS
-----------

Current maintainers:
 * Arunkumar Kuppuswamy (arunkumark) - https://www.drupal.org/user/2994023/
