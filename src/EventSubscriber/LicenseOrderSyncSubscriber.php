<?php

namespace Drupal\commerce_product_bundle_license\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product_bundle\Entity\BundleItemOrderItemInterface;

/**
 * Changes a license's state in sync with an order's workflow.
 */
class LicenseOrderSyncSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The license storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $licenseStorage;

  /**
   * Constructs a new LicenseOrderSyncSubscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->licenseStorage = $entity_type_manager->getStorage('commerce_license');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      // Events defined by state_machine, derived from the workflow defined in
      // commerce_order.workflows.yml.
      // See Drupal\state_machine\Plugin\Field\FieldType\StateItem::dispatchTransitionEvent()
      // TODO: Revisit these transitions and check they are correct.
      // Subscribe to events for reaching the states we support for activation.
      // We need our commerce_order.place.pre_transition method to run before
      // Commerce Recurring's, so that an initial order that purchases a
      // license subscription runs our method before
      // \Drupal\commerce_recurring\EventSubscriber's.
      // This is to ensure that the license is created here before it's
      // set on the subscription entity in
      // \Drupal\commerce_license\Plugin\Commerce\SubscriptionType::onSubscriptionCreate().
      'commerce_order.place.pre_transition' => ['onCartOrderTransition', 100],
      'commerce_order.validate.pre_transition' => ['onCartOrderTransition', -100],
      'commerce_order.fulfill.pre_transition' => ['onCartOrderTransition', -100],
      // Event for reaching the 'canceled' order state.
      'commerce_order.cancel.post_transition' => ['onCartOrderCancel', -100],
    ];
    return $events;
  }

  /**
   * Creates and activates a license in reaction to an order state change.
   *
   * We always create a license when the order goes through the 'place'
   * transition, regardless of which state that reaches, and at latest activate
   * it when the order reaches the 'completed' state.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event we subscribed to.
   */
  public function onCartOrderTransition(WorkflowTransitionEvent $event) {
    // Get the states we are leaving and reaching.
    $from_state = $event->getFromState()->getId();
    $reached_state = $event->getToState()->getId();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();

    $license_order_items = $this->getOrderItemsWithLicensedProducts($order);

    foreach ($license_order_items as $bundle_order_item) {
      $purchased_entity = $bundle_order_item->getPurchasedEntity();

      $license_type_plugin = $purchased_entity->get('license_type')->first()->getTargetInstance();

      if (is_object($license_type_plugin)) {

        $order_item = $bundle_order_item->getOrderItem();
        // Create the license the first time we come here, though allow for
        // something else to have created it for us already: this allows for
        // orders to be created programmatically with a configured license.
        if (empty($order_item->license->entity)) {
          // Create a new license. It will be in the 'new' state and so not yet
          // active.
          $license = $this->createFromBundleOrderItem($bundle_order_item);

          $license->save();

          // Set the license field on the order item so we have a reference
          // and can get hold of it in later events.
          $order_item->license = $license->id();
          $order_item->save();
        }
        else {
          // Get the existing license the order item refers to.
          $license = $order_item->license->entity;
        }

        // Now determine whether to activate it.
        $activate_license = FALSE;
        if ($reached_state == 'completed') {
          // Always activate the license when we reach the 'completed' state.
          $activate_license = TRUE;
        }
        else {
          // Activate the license in the 'place' transition if the product
          // variation type is configured to do so.
          // This then relies on onCartOrderCancel() to cancel the license if
          // the order itself is canceled later.
          $product_variation_type = $this->entityTypeManager->getStorage('commerce_product_variation_type')
            ->load($purchased_entity->bundle());
          $activate_on_place = $product_variation_type->getThirdPartySetting('commerce_license', 'activate_on_place');

          // We have to check state, because event can't tell transition.
          if ($activate_on_place && $from_state == 'draft') {
            $activate_license = TRUE;
          }
        }

        if (!$activate_license) {
          continue;
        }

        // Attempt to activate and confirm the license.
        // TODO: This needs to be expanded for synchronizable licenses.
        // TODO: how does a license type plugin indicate that it's not able to
        // activate? And how do we notify the order at this point?
        $transition = $license->getState()->getWorkflow()->getTransition('activate');
        $license->getState()->applyTransition($transition);
        $license->save();

        $transition = $license->getState()->getWorkflow()->getTransition('confirm');
        $license->getState()->applyTransition($transition);
        $license->save();
      }
    }
  }

  /**
   * Reacts to an order being cancelled.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event we subscribed to.
   */
  public function onCartOrderCancel(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();

    $license_order_items = $this->getOrderItemsWithLicensedProducts($order);

    foreach ($license_order_items as $order_item) {
      // Get the license from the order item.
      $license = $order_item->license->entity;

      // Cancel the license.
      $transition = $license->getState()->getWorkflow()->getTransition('cancel');
      $license->getState()->applyTransition($transition);
      $license->save();
    }
  }

  /**
   * Returns the order items from an order which are for licensed products.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface[]
   *   An array of the order items whose purchased products are for licenses.
   */
  protected function getOrderItemsWithLicensedProducts(OrderInterface $order) {
    $bundle_item_order_items = [];
    foreach ($order->getItems() as $order_item) {
      if ($order_item->bundle() === 'commerce_product_bundle_default') {
        if ($order_item->hasField('bundle_item_order_items')) {
          foreach ($order_item->get('bundle_item_order_items')
            ->referencedEntities() as $bundle_item_order_item) {
            $product_variation = $bundle_item_order_item->getPurchasedEntity();
            $license_type_plugin = $product_variation->get('license_type')->first()->getTargetInstance();
            if (is_object($license_type_plugin)) {
              $bundle_item_order_items[] = $bundle_item_order_item;
            }
          }
        }
      }
    }
    return $bundle_item_order_items;
  }

  /**
   * Create License for the Bundle order Items.
   *
   * @param \Drupal\commerce_product_bundle\Entity\BundleItemOrderItemInterface $bundle_item_order_item
   *   The bundle order entity.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   An object of the license.
   */
  public function createFromBundleOrderItem(BundleItemOrderItemInterface $bundle_item_order_item) {
    $purchased_entity = $bundle_item_order_item->getPurchasedEntity();

    // Take the license owner from the order, for the case when orders are
    // created for another user.
    $uid = $bundle_item_order_item->getOrderItem()->getOrder()->getCustomerId();

    $license = $this->createFromProductVariation($purchased_entity, $uid);

    return $license;
  }

  /**
   * Create License for the Bundle order Items.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariation $variation
   *   The bundle order entity.
   * @param int $uid
   *   The user id user entity.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   An object of the license.
   */
  public function createFromProductVariation(ProductVariation $variation, $uid) {
    // TODO: throw an exception if the variation doesn't have this field.
    $license_type_plugin = $variation->get('license_type')->first()->getTargetInstance();

    $license = $this->licenseStorage->create([
      'type' => $license_type_plugin->getPluginId(),
      'state' => 'new',
      'product_variation' => $variation->id(),
      'uid' => $uid,
      // Take the expiration type configuration from the product variation
      // expiration field.
      'expiration_type' => $variation->license_expiration,
    ]);

    // Set the license's plugin-specific configuration from the
    // product variation's license_type field plugin instance.
    $license->setValuesFromPlugin($license_type_plugin);

    return $license;
  }

}
